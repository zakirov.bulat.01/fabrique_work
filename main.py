from flask import *
from models.db import *
from datetime import *
from threading import Thread
import pytz
import time
import requests
import os
import json

app = Flask(__name__)
os.environ['WERKZEUG_RUN_MAIN'] = 'true'


# swagger
@app.route('/docs/', methods=['GET'])
def swagger():
    return redirect('https://app.swaggerhub.com/apis-docs/rastaManchi/API/1.0.0')


# client
@app.route('/add_client', methods=['POST'])
def clients_add():
    try:
        json_ = request.get_json(force=True)
        phone = json_['phone']
        code = json_['code']
        tag = json_['tag']
        time_zone = json_['time_zone']
        client_ = set_client(phone=phone, code=code, tag=tag, time_zone=time_zone)
        with open(f'logs/client_{client_.id}', 'w') as file:
            file.write(f'{str(datetime.now())} -- Client added! id={client_.id}')
        return 'Success'
    except Exception as e:
        return e


@app.route('/update_client', methods=['POST'])
def update_client():
    try:
        json_ = request.get_json(force=True)
        id = json_['id']
        phone = json_['phone']
        code = json_['code']
        tag = json_['tag']
        time_zone = json_['time_zone']
        update_client_by_phone(id=id, phone=phone, code=code, tag=tag, time_zone=time_zone)
        with open(f'logs/client_{str(id)}', 'a+') as file:
            file.write(f'\n{str(datetime.now())} -- client updated id={str(id)}')
        return 'Success'
    except Exception as e:
        return str(e)


@app.route('/client', methods=['DELETE'])
def delete_client():
    try:
        json_ = request.get_json(force=True)
        phone = json_['phone']
        code = json_['code']
        tag = json_['tag']
        time_zone = json_['time_zone']
        client_ = del_client(phone=phone, code=code, tag=tag, time_zone=time_zone)
        with open(f'logs/client_{client_.id}', 'a+') as file:
            file.write(f'\n{str(datetime.now())} -- client deleted id={client_.id}')
        return 'Success'
    except Exception as e:
        return str(e)


# mailing
def start_mailing(json_, clients, mailing_file):
    mailing_file.write(f'\n{str(datetime.now())} -- Thread started!')
    while True:
        mailing_status = True
        time.sleep(1)
        for client in clients:
            try:
                if client.status:
                    continue
            except Exception:
                pass
            client.status = False
            offset = timedelta(hours=client.time_zone)
            tz = timezone(offset)
            now_time = datetime.now(tz=tz)
            if datetime.strptime(json_['start_date'], '%Y-%m-%d %H:%M').astimezone(tz) < now_time < datetime.strptime(
                    json_['end_date'],
                    '%Y-%m-%d %H:%M').astimezone(tz):
                if client.tag == json_['filter'].split(",")[1] and int(client.code) == int(
                        json_['filter'].split(",")[0]):
                    id = set_message(start_date=datetime.now().strftime('%Y-%m-%d %H:%M'), msg_status=False,
                                     mailing_id=json_['id'],
                                     user_id=client.id)
                    payload = {"id": id.id, "phone": f'7{client.phone}', 'text': json_['msg']}
                    print(payload)
                    r = requests.post(f'https://probe.fbrq.cloud/v1/send/{id.id}', json=payload,
                                      headers={'accept': 'application/json', 'Content-Type': 'application/json',
                                               'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzkyMzExODIsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IlByb2dlcm1hbiJ9.MNAEGpoCJLfTZj74iTrNB3Cgc42mBId2kv5mJbX9zmE'})
                    json_request = json.loads(r.content)
                    if json_request['message'] == 'OK':
                        client.status = True
                        update_message(id=id.id, msg_status=True)
                        mailing_file.write(
                            f'\n{str(datetime.now())} -- message sended to client[{client.id}] -- message_id = {id.id}')
                        with open(f'logs/message_{id.id}', 'a+') as file:
                            file.write(
                                f'\n{str(datetime.now())} -- message sended! -- client_id = {id.user_id} -- mailing_id = {id.mailing_id} -- response - {json_request}')
                        with open(f'logs/client_{client.id}', 'a+') as file:
                            file.write(f'\n{str(datetime.now())} -- sent msg to client -- text={json_["msg"]}')
                    else:
                        mailing_file.write(f'\n{str(datetime.now())} -- snd message error -- client_id = {client.id}')
            else:
                mailing_status = False
        for client in clients:
            if not client.status:
                mailing_status = False
        if mailing_status:
            mailing_file.write(f'\n{str(datetime.now())} -- mailing successful ended!')
            return 'end'


@app.route('/mailing/<id>', methods=['DELETE'])
def delete_mailing(id):
    try:
        db_delete_mailing(id=id)
        return 'Success'
    except Exception as e:
        return str(e)


@app.route('/update_mailing', methods=['POST'])
def update_mailing():
    try:
        json_ = request.get_json(force=True)
        start_date = json_['start_date']
        msg = json_['msg']
        filter = json_['filter']
        end_date = json_['end_date']
        db_update_mailing(id=json_['id'], start_date=datetime.strptime(start_date, '%Y-%m-%d %H:%M'), msg=msg,
                          filter=filter, end_date=datetime.strptime(end_date, '%Y-%m-%d %H:%M'))
        with open(f'logs/mailing_{json_["id"]}', 'a+') as file:
            file.write(f'\n{str(datetime.now())} -- mailing updated!')
        return "Success"
    except Exception as e:
        return str(e)


@app.route('/mailings_info', methods=['GET'])
def mailings_info():
    try:
        json_ = {'status': 'Success', 'info': []}
        mailings = get_mailings()
        for mailing in mailings:
            mailing_json = {'mailing_info': {'id': mailing.id, 'start_date': mailing.start_date, 'msg': mailing.msg,
                                             'filter': mailing.filter, 'end_date': mailing.end_date}, 'messages': []}
            messages = get_msg_by_mailing_id(mailing_id=mailing.id)
            for message in messages:
                mailing_json['messages'].append(
                    {'id': message.id, 'start_date': message.start_date, 'msg_status': message.msg_status,
                     'mailing_id': message.mailing_id, 'user_id': message.user_id})
            json_['info'].append(mailing_json)
        return json_
    except Exception as e:
        return str(e)


@app.route('/mailing_info/<id>', methods=['GET'])
def mailing_info(id):
    try:
        json_ = {'status': 'Success', 'info': []}
        mailing = get_mailing(id=id)[0]
        mailing_json = {'mailing_info': {'id': mailing.id, 'start_date': mailing.start_date, 'msg': mailing.msg,
                                         'filter': mailing.filter, 'end_date': mailing.end_date}, 'messages': []}
        messages = get_msg_by_mailing_id(mailing_id=mailing.id)
        for message in messages:
            mailing_json['messages'].append(
                {'id': message.id, 'start_date': message.start_date, 'msg_status': message.msg_status,
                 'mailing_id': message.mailing_id, 'user_id': message.user_id})
        json_['info'].append(mailing_json)
        return json_
    except Exception as e:
        return str(e)


@app.route('/add_mailing', methods=['POST'])
def mailing_add():
    clients = get_clients()
    mailings = get_mailings()
    if clients:
        try:
            json_ = request.get_json(force=True)
            if mailings:
                json_['id'] = mailings[-1].id + 1
            else:
                json_['id'] = 1
            start_date = json_['start_date']
            msg = json_['msg']
            filter = json_['filter']
            end_date = json_['end_date']
            set_mailing(id=json_['id'], start_date=datetime.strptime(start_date, '%Y-%m-%d %H:%M'), msg=msg,
                        filter=filter, end_date=datetime.strptime(end_date, '%Y-%m-%d %H:%M'))
            mailing_file = open(f'logs/mailing_{json_["id"]}', 'w')
            mailing_file.write(f'{str(datetime.now())} -- mailing started!')
            clients = get_clients_filtered(code=str(json_['filter'].split(',')[0]),
                                           tag=str(json_['filter'].split(',')[1]))
        except Exception as e:
            return str(e)
        p1 = Thread(target=start_mailing, args=[json_, clients, mailing_file])
        p1.start()
        return 'Mailing started!!!'
    return 'Add clients to start!'


# flask
def start_flask():
    app.run(debug=False, use_reloader=False)


if __name__ == '__main__':
    start_flask()
