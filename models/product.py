from peewee import *
from models.utils import db


class mailing(Model):
    id = PrimaryKeyField()
    start_date = DateTimeField()
    msg = CharField()
    filter = CharField()
    end_date = DateTimeField()

    class Meta:
        database = db


class client(Model):
    id = PrimaryKeyField()
    phone = IntegerField()
    code = IntegerField()
    tag = CharField()
    time_zone = IntegerField()

    class Meta:
        database = db


class message(Model):
    id = PrimaryKeyField()
    start_date = DateTimeField()
    msg_status = BooleanField()
    mailing_id = IntegerField()
    user_id = IntegerField()

    class Meta:
        database = db
        order_by = ['-msg_status']
