from peewee import Model
from typing import List
from models.product import *
from models.utils import db


def connection(function: callable):
    def wrapper(*args, **kwargs):
        with db.connection():
            res = function(*args, **kwargs)
            db.commit()
            return res

    return wrapper


@connection
def create_table(*models: Model):
    for model in models:
        if not model.table_exists():
            model.create_table()


@connection
def drop_table(*models: Model):
    for model in models:
        if model.table_exists():
            model.drop_table()


@connection
def get_clients() -> List[client]:
    return list(client.select())


@connection
def get_clients_filtered(code: str, tag: str) -> List[client]:
    return list(client.select().filter(code=code, tag=tag))


@connection
def get_mailings() -> List[mailing]:
    return list(mailing.select())


@connection
def get_mailing(id: int) -> List[mailing]:
    return list(mailing.select().filter(id=id))


@connection
def get_msg() -> List[message]:
    return list(message.select())


@connection
def get_msg_by_mailing_id(mailing_id: int) -> List[message]:
    return list(message.select().filter(mailing_id=mailing_id))


@connection
def get_msg_by_user_id(user_id: int) -> List[message]:
    return list(message.select().filter(user_id=user_id))


@connection
def set_mailing(id: int, start_date, msg: str, filter: str, end_date):
    mailing.create(id=id, start_date=start_date, msg=msg, filter=filter, end_date=end_date)


@connection
def set_message(start_date, msg_status: bool, mailing_id: int, user_id: str):
    r = message.create(start_date=start_date, msg_status=msg_status, mailing_id=mailing_id, user_id=user_id)
    return r


@connection
def set_client(phone: int, code: int, tag: str, time_zone: int):
    client_ = client.create(phone=phone, code=code, tag=tag, time_zone=time_zone)
    return client_


@connection
def update_client_by_phone(id: int, phone: int, code: int, tag: str, time_zone: str):
    client.update(phone=phone, code=code, tag=tag, time_zone=time_zone).where(client.id == id).execute()


@connection
def db_update_mailing(id: int, start_date, msg: str, filter: str, end_date):
    mailing.update(start_date=start_date, msg=msg, filter=filter, end_date=end_date).where(mailing.id == id).execute()


@connection
def update_message(id: int, msg_status: str):
    message.update(msg_status=msg_status).where(message.id == id).execute()


@connection
def del_client(phone: int, code: int, tag: str, time_zone: str):
    models: List[client] = list(client.select().filter(phone=phone, code=code, tag=tag, time_zone=time_zone))
    models[0].delete_instance()
    return models[0]


@connection
def db_delete_mailing(id: int):
    models: List[mailing] = list(mailing.select().filter(id=id))
    models[0].delete_instance()


if __name__ == '__main__':
    create_table(mailing)
    create_table(client)
    create_table(message)
